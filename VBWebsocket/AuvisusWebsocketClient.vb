﻿Imports System
Imports System.Text
Imports System.Threading
Imports System.Threading.Tasks
Imports System.IO
Imports System.Net.WebSockets
Imports System.Windows

Public Delegate Sub OnWSConnectCallback(ByVal connectionMessage As String)
Public Delegate Sub OnWSErrorCallback(ByVal errorMessage As String)
Public Delegate Sub OnWSMessageCallback(ByVal message As String)

Public Class AuvisusWebsocketClient
    Private _socketThread As Thread
    Private connectionString As String
    Private socket As ClientWebSocket
    Private onWSMessageCallback As OnWSMessageCallback
    Private onWSErrorCallback As OnWSErrorCallback
    Private onWSConnectCallback As OnWSConnectCallback

    Public Sub New(ByVal connectionString As String, ByVal onWSMessageCallback As OnWSMessageCallback, ByVal onWSErrorCallback As OnWSErrorCallback, ByVal onWSConnectCallback As OnWSConnectCallback)
        Me.connectionString = connectionString
        Me.onWSMessageCallback = onWSMessageCallback
        Me.onWSErrorCallback = onWSErrorCallback
        Me.onWSConnectCallback = onWSConnectCallback
    End Sub

    Public Sub Start()
        _socketThread = New Thread(AddressOf SocketThreadFuncAsync)
        _socketThread.Start()
    End Sub

    Public Sub SendExternal(ByVal data As String)
        If socket IsNot Nothing AndAlso socket.State = WebSocketState.Open Then
            socket.SendAsync(Encoding.UTF8.GetBytes(data), WebSocketMessageType.Text, True, CancellationToken.None)
        Else
            MessageBox.Show("Not connected")
        End If
    End Sub

    Private Async Sub SocketThreadFuncAsync(ByVal state As Object)
        Do

            Using CSharpImpl.__Assign(socket, New ClientWebSocket())

                Try
                    Await socket.ConnectAsync(New Uri(connectionString), CancellationToken.None)
                    onWSConnectCallback($"Connected to {connectionString}")
                    Await Receive(socket)
                Catch ex As Exception
                    onWSErrorCallback($"ERROR - {ex.Message}")
                End Try
            End Using
        Loop While True
    End Sub

    Private Async Function Send(ByVal socket As ClientWebSocket, ByVal data As String) As Task
        ' Return Await socket.SendAsync(Encoding.UTF8.GetBytes(data), WebSocketMessageType.Text, True, CancellationToken.None)
    End Function

    Private Async Function Receive(ByVal socket As ClientWebSocket) As Task
        Dim buffer = New ArraySegment(Of Byte)(New Byte(2047) {})

        Do
            Dim result As WebSocketReceiveResult

            Using ms = New MemoryStream()

                Do
                    result = Await socket.ReceiveAsync(buffer, CancellationToken.None)
                    ms.Write(buffer.Array, buffer.Offset, result.Count)
                Loop While Not result.EndOfMessage

                If result.MessageType = WebSocketMessageType.Close Then Exit Do
                ms.Seek(0, SeekOrigin.Begin)

                Using reader = New StreamReader(ms, Encoding.UTF8)
                    onWSMessageCallback(Await reader.ReadToEndAsync())
                End Using
            End Using
        Loop While True
    End Function

    Private Class CSharpImpl
        <Obsolete("Please refactor calling code to use normal Visual Basic assignment")>
        Shared Function __Assign(Of T)(ByRef target As T, value As T) As T
            target = value
            Return value
        End Function
    End Class
End Class
