﻿'Imports AuvisusWebsocketClient
Imports System
Imports System.Windows

Partial Public Class MainWindow
    Inherits Window

    Private websocketClient As AuvisusWebsocketClient
    Private websocketAddress As String = "ws://localhost:6065"

    Private Sub startWebsocketClient()
        websocketClient = New AuvisusWebsocketClient(websocketAddress, AddressOf Me.OnMessageHandler, AddressOf Me.OnErrorHandler, AddressOf Me.OnConnectHandler)
        websocketClient.Start()

    End Sub

    Private Sub Button_Click_6(ByVal sender As Object, ByVal e As RoutedEventArgs)
        websocketAddress = wsAddress.Text
        wsAddress.IsEnabled = False
        startWebsocketClient()
        startConnection.Visibility = Visibility.Hidden
    End Sub

    Public Sub OnMessageHandler(ByVal message As String)
        Dispatcher.Invoke(New Action(Sub()
                                         incomingText.Text += message
                                     End Sub))
    End Sub

    Public Sub OnErrorHandler(ByVal message As String)
        Dispatcher.Invoke(New Action(Sub()
                                         connectionStatusLabel.Content = message
                                     End Sub))
    End Sub

    Public Sub OnConnectHandler(ByVal message As String)
        Dispatcher.Invoke(New Action(Sub()
                                         connectionStatusLabel.Content = message
                                     End Sub))
        MessageBox.Show(message)
    End Sub

    Private Sub Button_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
        commandTextBox.Text = "{""type"":""articles_synced"",""articles"":[{""id"":""1"",""name"":{""DE"":{""short"":""Maultaschen"",""addOn"":""mit Zwiebelschmälze""}},""price"":{""intern"":3.25}},{""id"":""2"",""name"":{""DE"":{""short"":""Spaghetti Arrabiata"",""addOn"":""scharf""}},""price"":{""intern"":2.95}},{""id"":""3"",""name"":{""DE"":{""short"":""Pommes"",""addOn"":""klein""}},""price"":{""intern"":1.2}},{""id"":""4"",""name"":{""DE"":{""short"":""Beilagensalat"",""addOn"":""mit Dressing""}},""price"":{""intern"":0.8}},{""id"":""5"",""name"":{""DE"":{""short"":""Reis"",""addOn"":""klein""}},""price"":{""intern"":1.1}},{""id"":""6"",""name"":{""DE"":{""short"":""Erdbeerquark"",""addOn"":""klein""}},""price"":{""intern"":0.7}},{""id"":""7"",""name"":{""DE"":{""short"":""Brötchen"",""addOn"":""Tafelbrötchen""}},""price"":{""intern"":0.6}},{""id"":""8"",""name"":{""DE"":{""short"":""Orange"",""addOn"":""Frisch in Bioqualität""}},""price"":{""intern"":0.6}},{""id"":""9"",""name"":{""DE"":{""short"":""Banane"",""addOn"":""Frisch in Bioqualität""}},""price"":{""intern"":0.6}},{""id"":""10"",""name"":{""DE"":{""short"":""Apfel"",""addOn"":""Frisch und regional""}},""price"":{""intern"":0.6}},{""id"":""11"",""name"":{""DE"":{""short"":""lemonaid"",""addOn"":""330ml""}},""price"":{""intern"":1.1}},{""id"":""12"",""name"":{""DE"":{""short"":""Snickers"",""addOn"":""Schokoriegel""}},""price"":{""intern"":0.8}},{""id"":""13"",""name"":{""DE"":{""short"":""Demo-Hauptgericht 1"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":2.8}},{""id"":""14"",""name"":{""DE"":{""short"":""Demo-Hauptgericht 2"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":3.8}},{""id"":""15"",""name"":{""DE"":{""short"":""Demo-Beilage 1"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":0.8}},{""id"":""16"",""name"":{""DE"":{""short"":""Demo-Beilage 2"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":1.2}},{""id"":""17"",""name"":{""DE"":{""short"":""Demo-Dessert 1"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":0.9}},{""id"":""18"",""name"":{""DE"":{""short"":""Demo-Dessert 2"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":1.3}},{""id"":""19"",""name"":{""DE"":{""short"":""Demo-Getränk 1"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":1.2}},{""id"":""20"",""name"":{""DE"":{""short"":""Demo-Getränk 2"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":1.8}},{""id"":""21"",""name"":{""DE"":{""short"":""Demo-Sonstiges 1"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":0.6}},{""id"":""22"",""name"":{""DE"":{""short"":""Demo-Sonstiges 2"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":0.7}},{""id"":""23"",""name"":{""DE"":{""short"":""Kleiner Salat"",""addOn"":""vom Buffet""}},""price"":{""intern"":1}},{""id"":""24"",""name"":{""DE"":{""short"":""Mittlerer Salat"",""addOn"":""vom Buffet""}},""price"":{""intern"":2}},{""id"":""25"",""name"":{""DE"":{""short"":""Großer Salat"",""addOn"":""vom Buffet""}},""price"":{""intern"":3}}]}"
    End Sub

    Private Sub Button_Click_1(ByVal sender As Object, ByVal e As RoutedEventArgs)
        commandTextBox.Text = "{" & vbLf & """type"": ""card_placed"", ""cardId"": ""a4157eab-2aac-4740-ad2a-158014a56a42""}"
    End Sub

    Private Sub Button_Click_2(ByVal sender As Object, ByVal e As RoutedEventArgs)
        If websocketClient IsNot Nothing Then
            websocketClient.SendExternal(commandTextBox.Text)
            outgoingText.Text += commandTextBox.Text
            outgoingText.Text += vbLf
        Else
            MessageBox.Show("Start a connection first")
        End If
    End Sub

    Private Sub Button_Click_3(ByVal sender As Object, ByVal e As RoutedEventArgs)
        commandTextBox.Text = "{""type"":""transaction_processed"",""transaction"":{""transactionResult"":""unknownArticle"",""auvisusTransactionId"":""d1b1a822-e534-4514-b8e2-bbba3209ba47"",""transactionId"":""8db6351d-d4ff-4a47-a52a-f663ee317c59"",""priceLevel"":""intern"",""totalCost"":5.32,""funds"":2.32,""basket"":[{""id"":""21"",""name"":{""DE"":{""short"":""Demo-Sonstiges 1"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":0.6}},{""id"":""23"",""name"":{""DE"":{""short"":""Kleiner Salat"",""addOn"":""vom Buffet""}},""price"":{""intern"":1}},{""id"":""6"",""name"":{""DE"":{""short"":""Erdbeerquark"",""addOn"":""klein""}},""price"":{""intern"":0.7}},{""id"":""15"",""name"":{""DE"":{""short"":""Demo-Beilage 1"",""addOn"":""Live in Demo eintrainiert""}},""price"":{""intern"":0.8}},{""id"":""10"",""name"":{""DE"":{""short"":""Apfel"",""addOn"":""Frisch und regional""}},""price"":{""intern"":0.6}}],""cardId"":""cc84567b-aed9-4208-8395-44e75f50710e""}}"
    End Sub

    Private Sub Button_Click_4(ByVal sender As Object, ByVal e As RoutedEventArgs)
        outgoingText.Text = ""
        incomingText.Text = ""
    End Sub

    Private Sub Button_Click_5(ByVal sender As Object, ByVal e As RoutedEventArgs)
        commandTextBox.Text = "{""type"":""payment_methods_synced"",""paymentMethods"":[{""id"":""1"",""name"":""Key Card"",""triggerOnCardPlaced"":true},{""id"":""2"",""name"":""Online Wallet"",""triggerOnCardPlaced"":true},{""id"":""3"",""name"":""EC"",""triggerOnCardPlaced"":false}],""defaultPaymentMethodId"":""1""}"
    End Sub
End Class
