# VB-dotNET-auvisus-connect-testingapp

This repository contains the code for a minimalistic Visual Basic WPF GUI 
which is capable of sending and receiving websocket messages to the auvisus websocket server.

## Setup 

* Download, unzip and run the latest release of the Visual basic dotNET auvisus connect testingapp: [VB dotNET auvisus connect testingapp v0.1.0](https://drive.google.com/file/d/1E4XuZj9L6xtTmqgtLtI-odFq0iaR1P1W/view?usp=sharing)
* Download and run the latest release of the [javascript auvisus connect testingapp v1.4.0](https://drive.google.com/file/d/1bv53riBwMr7qm7Q9JsRBaKHwPAATXf6X/view)
    * This should open two windows:
        * "visioncheckout Client": This implements the websocket server comparable to the real visioncheckout.
        * "Cashier system client": This implements all necessary client messages to communicate with the server.
    * Please close the window "Cashier system client" since this is replaced by our dotNET testingapp


You can now implement your own websocket client, integrate it into the cashier PC and test its endpoints using the "visioncheckout Client".


## How should it look like? 

This is a gif that shows how the process looks like for the c# .net application. The VB app works analogously.

![demo gif](https://gitlab.com/daniel.pollithy/dotnet-auvisus-connect-testingapp/-/raw/main/2021-07-02-15-04-17.gif)
